{
    "LanguageName": "Norsk (bokmål)",

    "LoadingText": "Laster...",
    "OkButton": "OK",
    "CancelButton": "Avbryt",
    "YesButton": "Ja",
    "NoButton": "Nei",
    "ContinueButton": "Fortsett",
    "BackButton": "Tilbake",
    "ExitButton": "Gå ut",
    "EditButton": "Rediger",
    "SaveButton": "Lagre",
    "CopyButton": "Kopier",
    "ChooseFileButton": "Velg fil...",
    "DefaultValueText": "{} (standard)",
    "ErrorPopup": {
        "TitleText": "Feil!"
    },
    
    "TitleScreen": {
        "DemoText": "DEMO",
        "SingleplayerButton": "Enkeltspiller",
        "MultiplayerButton": "Flerspiller",
        "TutorialButton": "Opplæring",
        "SettingsButton": "Innstillinger",
        "CreditsButton": "Krediteringsliste",
        "QuitButton": "Avslutt spill"
    },
    
    "Singleplayer": {
        "YourWorldsText": "Dine verdener:",
        "NoWorldsText": "Det er ingen verdener å laste...",
        "InvalidWorldButton": "{} (ugyldig)",
        "OldWorldButton": "{} (gammel)",
        "NewWorldButton": "Ny verden",
        "WorldConversionPopup": {
            "TitleText": "Vil du kovertere verdenen til det nyeste formatet?",
            "OldWorldVersionText": " \"{}\" kan ikke lastes fordi verdenen bruker en gammel versjon av verdensformatet ({}), som er inkompatibel med den nåværende spillversjonen ({}).",
            "OldWorldVersionlessText": "\"{}\" kan ikke lastes fordi verdenen bruker en gammel versjon av verdensformatet, som er inkompatibel med den nåværende spillversjonen",
            "ConversionPromptText": "Vil du konvertere verdenen til den nyeste versjonen? Du får muligheten til å sikkerhetskopiere først."
        },
        "InvalidWorldPopup": {
            "TitleText": "Ugyldig verden!",
            "InvalidVersionText": " \"{}\" kan ikke bli lastet fordi verdenen er enten fra en fremtidig versjon av 4D Miner, eller så har den et ugyldig versjonstall."
        }
    },

    "Multiplayer":{
        "ServerAddressText": "Serveradresse:",
        "DisplayNameText": "Visningsnavn:",
        "DisplayNameInput": "Spiller",
        "UuidText": "UUID:",
        "UuidDescriptionText": "UUID-en din er en hemmelig kode som er generert den første gangen du åpner denne menyen.\nUUIDer lar deg lagre hvor langt du har kommet på forskjellige servere. \n\nALDRI del denne koden med noen.\nDu kan overføre en UUID fra en tidligere installasjon ved å erstatte/redigere \"uuid.txt\" filen.",
        "JoinButton": "Bli med!",
        "ChangeSkinButton": "Bytt kostyme",
        "Status": {
            "ResolvingHostText": "Finner vert...",
            "ConnectingText": "Kobler til...",
            "AuthenticatingText": "Autentiserer...",
            "RemoteDisconnectedText": "Feil: Koblingen ble avbrutt av serveren",
            "FailedToConnectText": "Feil: Kunne ikke koble til serveren.",
            "ErrorText": "Noe har gått galt.",
            "LoadingTerrainText": "Laster terreng...",
            "QueuePositionText": "Din posisjon i køen: {}",
            "InvalidResponseText": "Feil: Ugyldig serverrespons",
            "IncompatibleVersionText": "Feil: Versjonen til spillet er ikke den samme som versjonen til serveren.\n\nKompatible versjoner: {}",
            "ServerFullText": "Feil: Serveren er full.",
            "LostConnectionText": "Feil: Mistet tilkoblingen"
        }
    },
    
    "CreateWorld": {
        "WorldNameText": "Verdensnavn:",
        "WorldNameInput": "Ny verden",
        "WorldSeedText": "Verdensfrø:",
        "FlatWorldCheckbox": "Flat verden",
        "GenerateCavesCheckbox": "Hulegenerering (dette kan sakke ned genereringsprosessen)",
        "CaveSizeSlider": "Hulestørrelse: {}%",
        "CaveFrequencySlider": "Hulemengde: {}%",
        "CaveDetailLevelSlider": "Huledetaljer: {} (påvirker ytelsen kraftig)",
        "BiomeSizeSlider": "Biomstørrelse: {}%",
        "TerrainAmplificationSlider": "Biomforsterkning: {}%",
        "CreateNewWorldButton": "Lag en ny verden!",
        "NoNamePopup": {
            "NoNameText": "Du kan ikke lage en verden uten navn!"
        }
    },

    "WorldConverter": {
        "CloseWarningText": "IKKE LUKK SPILLET!",
        "BackupWorldTitle": "{} (sikkerhetskopi)",
        "BackupStatusText": "Sikkerhetskopierer \"{}\"...",
        "ConversionStatusText": "Konverterer \"{}\" fra versjon {} til versjon {}...",
        "ConversionFinishedText": "Ferdig!",
        "BackupPopup": {
            "TitleText": "Vil du sikkerhetskopiere filene fra \"{}\" ?",
            "PromptText": "Lagringsdataene fra \"{}\" kan bli skadet under konverteringsprosessen, vil du sikkerhetskopiere først?"
        },
        "ErrorPopup": {
            "GenericConversionErrorText": "Noe gikk galt under konverteringen til versjon {}.",
            "InvalidObjectText": "Kan ikke kopiere ugyldig objekt \"{}\" (forventet et bibliotek eller en vanlig fil).",
            "CopyErrorText": "Kan ikke kopiere objekt \"{}\": \"{}\"",
            "MissingChunksDirText": "Manglende \"seksjon \"bibliotek.",
            "UnableToOpenFileText": "Kan ikke åpne denne filen: \"{}\"."
        },
        "ConversionStatus": {
            "CleaningUpText": "Rydder opp..."
        }
    },
    
    "WorldGen": {
        "ProgressText": "{}/{} seksjoner lastet"
    },

    "Settings": {
        "ControlsButton": "Knappinnstillinger",
        "FullscreenButton": {
            "EnterText": "Fullskjerm-modus",
            "ExitText": "Gå ut av fullskjerm-modus"
        },
        "RenderDistanceSlider": "Visningssavstand: {}",
        "ScrollSensitivitySlider": "Musehjulssensitivitet:",
        "LookSensitivitySlider": "Musesensistivitet:",
        "InvertLookXCheckbox": "Reverser musebevegelse på X aksen",
        "InvertLookYCheckbox": "Reverser musebevegelse på Y aksen",
        "FovSlider": {
            "LabelText": "Synsfelt: {}",
            "ValueText": "{} grader"
        },
        "DifficultySlider": {
            "LabelText": "Vanskelighetsgrad: {}",
            "EasyText": "Lett (ingen monstre)",
            "NormalText": "Normal",
            "HardText": "Vanskelig"
        },
        "SmoothLightingCheckbox": "Jevn belysning",
        "ShadowsCheckbox": "Skygger",
        "ColoredLightsCheckbox": "Fargede lys",
        "Forg": {
            "LabelText": "Froks: {}",
            "Names": {
                "Frank": "Frank",
                "Fernanda": "Fredrikke"
            }
        },
        "Audio": {
            "LabelText": "Lydinnstillinger:",
            "GlobalVolumeSlider": "Globalt Lydnivå:",
            "MusicVolumeSlider": "Lydnivået til musikken:",
            "CreatureVolumeSlider": "Lydnivået til vesenene:",
            "AmbienceVolumeSlider": "Bakgrunnslydnivå:"
        },
        "Multiplayer": {
            "LabelText": "Flerspiller:",
            "ChatCheckbox": "Vis chatvinduet",
            "NametagsCheckbox": "Vis spillernavn",
            "SkinsCheckbox": "Vis egendefinerte spillerkostymer"
        }
    },

    "Tutorial": {
        "Slideshow": {
            "Slide1": {
                "Text1": "Denne opplæringen er designet for å hjelpe deg med å forstå den fjerde dimensjonen.\n\n\n\nVed å sammenlikne forskjellene mellom 2D og 3D kan du få en forståelse for hvordan en 3D verden er i forhold til en verden som har fire dimensjoner."
            },
            "Slide2": {
                "Text1": "Dette er en froks (ja, en FROKS) den heter {}",
                "Text2": "Frokser er 2D vesener, som betyr at de bare kan eksistere i et virkelighetsplan med to dimensjoner, lengde og bredde, men det betyr ikke at de ikke kan utforske verdener som har dybde!\n\n\n\nFrokser kan bevege seg rundt i ethvert 2D tverrsnitt av 3D verdenen de befinner seg i."
            },
            "Slide3": {
                "Text1": "Froksene har også en unik evne som lar dem rotere virkelighetsplanet sitt, så de kan utforske flere tverrsnitt av 3D verdenen!",
                "Text2": "I den neste delen skal du få lov til å kontrollere {} og oppleve denne evnen selv!"
            },
            "Slide4": {
                "Text1": "Den samme logikken gjelder også for 3D og 4D. \n\n\nDu kan bruke musehjulet til å rotere 3D tverrsnittet ditt av den firedimensjonale verdenen, og se helt forskjellige deler av terrenget!",
                "Text2": "Nå er du klar til å spille 4D Miner, hvis du har problemer med å navigere gjennom den firedimensjonale verden, let etter…",
                "Text3": "kompasset!"
            }
        },
        "ForgGame": {
            "2DViewText": "Bruk WASD eller pilknappene til å bevege froksen {}.\n\nBruk musehjulet til å rotere 2D tverrsnittet!",
            "3DViewText": "Klikk og dra for å justere 3D-synet"
        }
    },

    "Game": {
        "Chat": {
            "LabelText": "Chat:"
        },
        "CraftingMenu": {
            "LabelText": "Håndverk:"
        },
        "Compass": {
            "PositionText": "Posisjon: {0}, {1}, {2}, {3}",
            "FacingDirectionText": "Retning: {0}, {1}, {2}, {3}",
            "XText": "X: {}",
            "YText": "Y: {}",
            "ZText": "Z: {}",
            "WText": "W: {}"
        },
        "Chest": {
            "LabelText": "Kiste:"
        },
        "Sign": {
            "LabelText": "Skilt:"
        }
    },

    "Pause": {
        "PausedText": "Pauset...",
        "BackToGameButton": "Tilbake til spillet...",
        "QuitToTitleButton": "Avslutt og gå til startssiden"
    },

    "DeathScreen": {
        "RetryButton": "Prøv igjen..."
    },

  "Items": {
    "Air": "Luft",
    "Grass": "Gress",
    "Dirt": "Jord",
    "Stone": "Stein",
    "Wood": "Tre",
    "Leaf": "Løv",
    "SemiMoltenRock": "Halvsmeltet stein",
    "IronOre": "Jernmalm",
    "DeadlyOre": "Livsfarlig malm",
    "Chest": "Kiste",
    "MidnightGrass": "Midnattsgress",
    "MidnightSoil": "Midnattsjord",
    "MidnightStone": "Midnattstein",
    "MidnightWood": "Midnattstre",
    "MidnightLeaf": "Midnattsløv",
    "Bush": "Busk",
    "MidnightBush": "Midnattsbusk",
    "RedFlower": "Rød blomst",
    "WhiteFlower": "Hvit blomst",
    "BlueFlower": "Blå blomst",
    "TallGrass": "Høyt gress",
    "Sand": "Sand",
    "Sandstone": "Sandstein",
    "Cactus": "Kaktus",
    "Snow": "Snø",
    "Ice": "Is",
    "SnowyBush": "Snødekket busk",
    "Glass": "Glass",
    "SolenoidOre": "Solenoidemalm",
    "SnowyLeaf": "Snødekkede blader",
    "Pumpkin": "Gresskar",
    "JackOLantern": "Gresskarlykt",
    "PalmWood": "Palmetre",
    "PalmLeaf": "Palmeblader",
    "Water": "Vann",
    "Lava": "Lava",
    "VolcanicRock": "Vulkansk stein",
    "GrassSlab": "Halvblokk av gress",
    "DirtSlab": "Halvblokk av jord",
    "StoneSlab": "Halvblokk av stein",
    "WoodSlab": " Halvblokk av tre",
    "SemiMoltenRockSlab": "Halvblokk av halvsmeltet stein",
    "MidnightStoneSlab": "Halvblokk av midnattstein",
    "MidnightWoodSlab": "Halvblokk av midnattstre",
    "SandstoneSlab": "Halvblokk av sandstein",
    "SnowSlab": "Halvblokk av snø",
    "IceSlab": "Halvblokk av is",
    "GlassSlab": "Halvblokk av glass",
    "PalmWoodSlab": "Halvblokk av palmetre",
    "VolcanicRockSlab": "Halvblokk av vulkansk stein",
    "Stick": "Pinne",
    "Hammer": "Hammer",
    "IronPick": "Jernhakke",
    "DeadlyPick": "Livsfarlig hakke",
    "IronAxe": "Jernøks",
    "DeadlyAxe": "Livsfarlig øks",
    "Ultrahammer": "Ultrahammer",
    "SolenoidCollector": "Solenoidesamler",
    "Rock": "Småstein",
    "Hypersilk": "Hypersilke",
    "IronBars": "Jernbarre",
    "DeadlyBars": "Barre av livsfarlig malm",
    "SolenoidBars": "Barre av solenoidemalm",
    "Compass": "Kompass",
    "4DGlasses": "4D briller",
    "KleinBottle": "Kleinflaske",
    "HealthPotion": "Helsedrikk",
    "RedLens": "Rød linse",
    "GreenLens": "Grønn linse",
    "BlueLens": "Blå linse",
    "Alidade": "Siktelinjal",
    "Sign": "Skilt",
    "Bucket": "Bøtte",
    "WaterBucket": "Vannbøtte",
    "LavaBucket": "Lavabøtte"
  }
}

